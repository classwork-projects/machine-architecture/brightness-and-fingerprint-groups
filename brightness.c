#include <stdio.h>
#include <stdlib.h>
#include "pnmrdr.h"
#include "brightness.h"

void ComputeAvgBrightness(FILE *pgmFile) {
    TRY
        Pnmrdr_T rdr = Pnmrdr_new(pgmFile);
        Pnmrdr_mapdata data = Pnmrdr_data(rdr);

        if (data.type != Pnmrdr_gray) { //not a gray pgm image
            fprintf(stderr, "Not a proper pgm file \n");
            Pnmrdr_free(&rdr);
            exit(EXIT_FAILURE);
        }
        //Add each pixels brightness to running total
        for (unsigned int i = 0; i < data.width; i++) {
            for (unsigned int j = 0; j < data.height; j++) {
                Average_Brightness += Pnmrdr_get(rdr);
                Pixel_Count += 1;
            }
        }
        Average_Brightness = (Average_Brightness / Pixel_Count) / data.denominator; //Compute Average Brightness
        Pnmrdr_free(&rdr);

    //Possible Exceptions
    EXCEPT(Pnmrdr_Count)
        fprintf(stderr, "Count Error\n");
        exit(EXIT_FAILURE);

    EXCEPT(Pnmrdr_Badformat)
        fprintf(stderr, "Incorrect Format\n");
        exit(EXIT_FAILURE);

    END_TRY;
}

int main(int argc, char* argv[]){
    printf("argc: %d\n", argc);
    FILE *pgmfile;
    if (argc == 1) //Run with standard input
    {
        pgmfile = stdin;
        if(pgmfile == NULL)
        {
            fprintf(stderr, "Incorrect Format\n");
            exit(EXIT_FAILURE);
        }
    }
    else if (argc > 2)
    {
        fprintf(stderr, "Too many arguments\n");
        exit(EXIT_FAILURE);
    }
    else //Run with pgm file
    {
        pgmfile = fopen(argv[1], "rb"); //Opens File
        if (pgmfile == NULL) {
            fprintf(stderr, "%s: Could not open file %s for reading. Closing file and doing nothing.\n",
                    argv[0], argv[1]);
            return (1);
        }
    }
    ComputeAvgBrightness(pgmfile); //Call to brightness Function
    printf("%0.3f \n", Average_Brightness);
    fclose(pgmfile); //Closes File
    return 0;
}

