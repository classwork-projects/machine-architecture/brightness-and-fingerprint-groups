#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <list.h>
#include <stdbool.h>
#include <string.h>
#include <mem.h>
#include "atom.h"
#include "table.h"
#include "assert.h"


// given a file fingerprints and names it will return a table containing all fingerprint/name pairs
Table_T get_table()
{
    // keys: fingerprint groups, values: names
    Table_T fingerprint_table = Table_new(0, NULL, NULL);
    // read stdin and dynamically allocate char array to build table
    size_t buffer_len = 10;
    char *buffer = malloc(buffer_len);
    while (fgets(buffer, buffer_len, stdin) != NULL)
    {
        while (strcspn(buffer, "\n") == buffer_len - 1)
        {
            // read stdin until a newline is reached
            size_t old_len = buffer_len;
            size_t new_len = old_len * 2;
            char *new_buffer = realloc(buffer, new_len);

            if (new_buffer != NULL)
            {
                buffer = new_buffer;
                buffer_len = new_len;
            } else
            {
                fprintf(stderr, "Realloc failed.\n");
                exit(EXIT_FAILURE);
            }
            // if no more stdin
            if (fgets(buffer + old_len - 1, new_len - old_len + 1, stdin) == NULL)
            {
                break;
            }
        }

        // parse input
        buffer[strcspn(buffer, "\n")] = '\0';
        char *current_fingerprint = strtok(buffer, " "); // get fingerprint from buffer
        char *current_name = strtok(NULL, "\0"); // continue with current buffer to get name
        if (current_name == NULL || current_fingerprint == NULL)
        {
            fprintf(stderr, "Error parsing input\n");
            exit(EXIT_FAILURE);
        }

        // store data in table
        List_T fingerprint_names = Table_get(fingerprint_table, Atom_string(current_fingerprint));
        // if fingerprint doesn't already exists in table
        if (fingerprint_names == NULL)
        {
            fingerprint_names = List_list((char *) Atom_string(current_name), (void *) NULL);
        } else
        {   // add name to existing fingerprint list
            fingerprint_names = List_push(fingerprint_names, (char *) Atom_string(current_name));
        }
        Table_put(fingerprint_table, Atom_string(current_fingerprint),
                  fingerprint_names); // put name and fingerprint in table
    }

    return fingerprint_table;
}


// given a table of fingerprints with name lists,
// print the fingerprint groups with at least 2 names matching the fingerprint
void print_fingerprint_groups(Table_T table)
{
    bool initial = true; // determines if we are printing the first group of names and if so prevents printing a new line
    // convert table to array where even indices store a fingerprint(keys) and odd indices store names(values)
    void **tableAsArray = Table_toArray(table, NULL);
    int tableSize = Table_length(table);
    // for each fingerprint
    for (int i = 0; i < tableSize * 2; i += 2)
    {
        List_T currentNames = tableAsArray[i + 1];
        int listLen = List_length(currentNames);
        if (listLen > 1)
        {
            if (!initial)
                printf("\n");
            else
                initial = false;

            // for each name with current fingerprint
            for (int j = 0; j < listLen; j++)
            {
                void *n;
                currentNames = List_pop(currentNames, &n);
                printf("%s\n", (const char *) n);
            }
        }
    }
    FREE(tableAsArray);
}


int main(int argc, char *argv[])
{
    (void) argv;
    if (argc > 1)
    {
        fprintf(stderr, "Too many arguments\n");
        exit(EXIT_FAILURE);
    }

    Table_T fingerprint_table = get_table();
    print_fingerprint_groups(fingerprint_table);
    Table_free(&fingerprint_table);

    return 0;
}
