# Brightness and Fingerprint Groups

This project was developed for the purpose of learning Dave Hanson’s C library and file I/O in C. It consists of two separate programs: Brightness and Fingerprint Groups.  

## Brightness
Given a greyscale .pgm image, prints the image's average brightness in decimal notation.

**Input**  
• If an argument is given, it should be the name of a portable graymap file (in pgm format).  
• If no argument is given, brightness reads from standard input, which should contain a portable graymap.     
<br />

## Fingerprint Groups
Given a set of names with fingerprints, identify groups
of names that share a fingerprint. 

**Input**  
A sequence of lines from standard input only, where each line has the following format:
• The line begins with one or more non-whitespace characters. This sequence of characters is the fingerprint.  
• The fingerprint is followed by one or more whitespace characters, not including newline.  
• The name begins with the next non-whitespace character and continues through the next newline. A name may contain whitespace, but a name never contains a newline.  
• A fingerprint is at most 512 characters long (2048 bits represented in hexadecimal notation), but there is no a priori upper bound on the length of a name.  
• If a fingerprint is associated with exactly one name, the name and fingerprint is ignored.  
• If a fingerprint is associated with two or more names, those names constitute a fingerprint group. A fingerprint group always has at least two members.  

**Output**  
• If there are no fingerprint groups, print nothing.  
• If there is exactly one fingerprint group, print it.  
• If there are multiple fingerprint groups, print them separated by newlines.  

Fgroups can solve any sort of problem where a dataset of variables needs to be grouped by a matching fingerprint. Such issues could include things such as in a wildlife database to group animals that are of the same species, in a university database to keep track of which students are taking what classes (where each student would have to be a fingerprint), or searching in a medical database to link patients with a matching health condition. Any application requires that the variables are unique, but they share a single group value.

